﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class BlockSecureLootPickupSigned : BlockSecureLootSigned
{
    private float _takeDelay = 2f;

    private readonly BlockActivationCommand[] _activationCommands = new BlockActivationCommand[]
      {
            new BlockActivationCommand("Search", "search", false),
            new BlockActivationCommand("lock", "lock", false),
            new BlockActivationCommand("unlock", "unlock", false),
            new BlockActivationCommand("keypad", "keypad", false),
            new BlockActivationCommand("pick", "unlock", false),
            new BlockActivationCommand("edit", "pen", false),
            new BlockActivationCommand("take", "hand", false)
      };

    public override void Init()
    {
        base.Init();

        _takeDelay = !Properties.Values.ContainsKey("TakeDelay") ? 2f : StringParsers.ParseFloat(Properties.Values["TakeDelay"], 0, -1, NumberStyles.Any);
    }

    public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
    {
        TileEntitySecureLootContainerSigned tileEntitySecureLootContainerSigned = _world.GetTileEntity(_clrIdx, _blockPos) as TileEntitySecureLootContainerSigned;
        if (tileEntitySecureLootContainerSigned == null)
        {
            return new BlockActivationCommand[0];
        }
        string _steamID = GamePrefs.GetString(EnumGamePrefs.PlayerId);
        PersistentPlayerData playerData = _world.GetGameManager().GetPersistentPlayerList().GetPlayerData(tileEntitySecureLootContainerSigned.GetOwner());
        bool flag = !tileEntitySecureLootContainerSigned.IsOwner(_steamID) && (playerData != null && playerData.ACL != null) && playerData.ACL.Contains(_steamID);
        _activationCommands[0].enabled = true;
        _activationCommands[1].enabled = (!tileEntitySecureLootContainerSigned.IsLocked() && (tileEntitySecureLootContainerSigned.IsOwner(_steamID) || flag));
        _activationCommands[2].enabled = (tileEntitySecureLootContainerSigned.IsLocked() && tileEntitySecureLootContainerSigned.IsOwner(_steamID));
        _activationCommands[3].enabled = ((!tileEntitySecureLootContainerSigned.IsUserAllowed(_steamID) && tileEntitySecureLootContainerSigned.HasPassword() && tileEntitySecureLootContainerSigned.IsLocked()) || tileEntitySecureLootContainerSigned.IsOwner(_steamID));
        _activationCommands[4].enabled = (this.lockPickItem != null && tileEntitySecureLootContainerSigned.IsLocked() && !tileEntitySecureLootContainerSigned.IsOwner(_steamID));
        _activationCommands[5].enabled = tileEntitySecureLootContainerSigned.IsUserAllowed(_steamID) || _world.IsMyLandProtectedBlock(_blockPos, _world.GetGameManager().GetPersistentLocalPlayer(), false);
        _activationCommands[6].enabled = _world.IsMyLandProtectedBlock(_blockPos, _world.GetGameManager().GetPersistentLocalPlayer(), false) && _takeDelay > 0.0;

        return _activationCommands;
    }

    public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
    {
        TileEntitySecureLootContainerSigned tileEntitySecureLootContainerSigned = _world.GetTileEntity(_cIdx, _blockPos) as TileEntitySecureLootContainerSigned;
        if (tileEntitySecureLootContainerSigned == null)
        {
            return false;
        }

        EntityPlayerLocal entityPlayerLocal = _player as EntityPlayerLocal;
        LocalPlayerUI uiForPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);
        if (_indexInBlockActivationCommands == 6)
        {
            if (!tileEntitySecureLootContainerSigned.IsEmpty())
            {
                GameManager.ShowTooltipWithAlert(entityPlayerLocal, Localization.Get("ttEmptyLootContainerBeforePickup"), "ui_denied");
                return false;
            }
            TakeItemWithTimer(_cIdx, _blockPos, _blockValue, _player);
            return true;
        }

        return base.OnBlockActivated(_indexInBlockActivationCommands, _world, _cIdx, _blockPos, _blockValue, _player);
    }

    public void TakeItemWithTimer(int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
    {
        if (_blockValue.damage > 0)
        {
            GameManager.ShowTooltipWithAlert(_player as EntityPlayerLocal, Localization.Get("ttRepairBeforePickup"), "ui_denied");
        }
        else
        {
            LocalPlayerUI playerUi = (_player as EntityPlayerLocal).PlayerUI;
            playerUi.windowManager.Open("timer", true, false, true);
            XUiC_Timer childByType = playerUi.xui.GetChildByType<XUiC_Timer>();
            TimerEventData _eventData = new TimerEventData
            {
                Data = new object[4]
                {
                     _cIdx,
                     _blockValue,
                     _blockPos,
                     _player
                }
            };
            _eventData.Event += new TimerEventHandler(this.EventData_Event);
            childByType.SetTimer(_takeDelay, _eventData);
        }
    }

    private void EventData_Event(object obj)
    {
        World world = GameManager.Instance.World;

        var eventData = obj as TimerEventData;

        object[] objArray = (object[])eventData.Data;

        int _clrIdx = (int)objArray[0];
        BlockValue blockValue = (BlockValue)objArray[1];

        Vector3i vector3i = (Vector3i)objArray[2];
        BlockValue block = world.GetBlock(vector3i);

        EntityPlayerLocal entityPlayerLocal = objArray[3] as EntityPlayerLocal;
        if (block.damage > 0)
            GameManager.ShowTooltipWithAlert(entityPlayerLocal, Localization.Get("ttRepairBeforePickup"), "ui_denied");
        else if (block.type != blockValue.type)
        {
            GameManager.ShowTooltipWithAlert(entityPlayerLocal, Localization.Get("ttBlockMissingPickup"), "ui_denied");
        }
        else
        {
            TileEntitySecureLootContainerSigned tileEntity = world.GetTileEntity(_clrIdx, vector3i) as TileEntitySecureLootContainerSigned;
            if (tileEntity.IsUserAccessing())
            {
                GameManager.ShowTooltipWithAlert(entityPlayerLocal, Localization.Get("ttCantPickupInUse"), "ui_denied");
            }
            else
            {
                LocalPlayerUI uiForPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);
                ItemStack itemStack = new ItemStack(block.ToItemValue(), 1);
                if (!uiForPlayer.xui.PlayerInventory.AddItem(itemStack, true))
                    uiForPlayer.xui.PlayerInventory.DropItem(itemStack);
                world.SetBlockRPC(_clrIdx, vector3i, BlockValue.Air);
            }
        }
    }
}
